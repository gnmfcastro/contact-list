import ContactController from "./modules/ContactController"

class Controller{

    constructor(){
        this.contact = new ContactController();
    }
    
}

export default Controller;