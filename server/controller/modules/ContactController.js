import Contact from "../../model/modules/Contact/Contact"
import ContactDAO from "../../model/modules/Contact/ContactDAO"

class ContactController{

    constructor(){
        this._contactDAO = new ContactDAO(Contact);
    }

    getAll(req,res){
        this._contactDAO.findAll((err, users) => {
            if(!err){
                res.status(200).json(users);
            }else{
                res.status(500).send();
            }
        });
    }

    getById(req,res){
        if(req.params.id){
            let id = req.params.id;
            this._contactDAO.findById(id,(err, user) =>{
                if(!err){
                    if(user){
                        res.status(200).json(user);
                    }else{
                        res.status(404).send();
                    }
                }else{
                    res.status(500).send();
                }
            });
        }else{
            res.status(400).send({'400':'It should receive a id param'});
        }
    }

    getByName(req,res){
        if(req.params.name){
            let name = req.params.name;
            this._contactDAO.findByName(name,(err, users) => {
                if(!err){
                    if(users.length > 0){
                        res.status(200).json(users);
                    }else{
                        res.status(404).send();
                    }
                }else{
                    res.status(500).send();
                }
            });
        }else{
            res.status(400).send({'400':'It should receive a name param'});
        }
    }

    postNewContact(req,res){
        if(req.body){
            let contact = req.body; 

            contact = this._contactDAO.save(contact);

            if(contact){
                res.status(200).send()
            }else{
                res.status(500).send();
            }
            
        }else{
            res.status(400).send({'400':'It should receive contact'});
        }
    }

}

export default ContactController;