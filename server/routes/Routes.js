import ContactRoute from "./modules/ContactRoute"
import bodyParser   from "body-parser"

class Routes{

    constructor(){
        this._headers = {
            "Access-Control-Allow-Origin":"*",
            "Access-Control-Allow-Headers":"Origin, X-Requested-With, Content-Type, Accept"
        }
    }   

    publish(app, controller){

        app.use((req, res, next) => {
            res.set(this._headers);
            next();
        });

        app.use(bodyParser.json());

        ContactRoute.publish(app,controller.contact);
        
    }

    get headers(){
        return this._headers;
    }
}

export default Routes