class ContactRoute{
    static publish(app, controller){

        app.get("/contacts", (req, res) => controller.getAll(req, res));
        app.post("/contact", (req,res) => controller.postNewContact(req,res));
        app.get("/contact/:name", (req, res) => controller.getByName(req, res));
        app.get("/contact/id/:id", (req, res) => controller.getById(req, res));

    }
}

export default ContactRoute