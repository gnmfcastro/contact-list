import request from 'supertest';

describe('loading express', () => {
  var server;
  beforeEach(() => {
    server = require('../app');
  });

  it('responds to /contacts', function testContacts(done) {
  request(server)
    .get('/contacts')
    .expect(200, done);
  });
  it('404 everything else', function testAnyPath(done) {
    request(server)
      .get('/foo/bar')
      .expect(404, done);
  });
  it('404 if didnt pass name to contact', function testContact(done) {
    request(server)
      .get('/contact')
      .expect(404, done);
  });
  it('200 when finding name', function testContact(done) {
    request(server)
      .get('/contact/Angel')
      .expect(200, done);
  });  
  it('500 if sensing any open id to db', function testContact(done) {
    request(server)
      .get('/contact/id/123123')
      .expect(500, done);
  });
});