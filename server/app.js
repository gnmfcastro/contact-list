import express          from "express"
import Routes           from "./routes/Routes"
import Model            from "./model/Model"
import Controller       from "./controller/Controller"
import morgan           from "morgan"

let app = express();

if(process.env.NODE_ENV){
  if(process.env.NODE_ENV === 'test'){
     app.config = require('./config/test.json');
  }else if(process.env.NODE_ENV === 'dev'){
    app.config = require('./config/dev.json');
  } 
}else{
   app.config = require('./config/default.json');
}

let controller = new Controller();
let routes = new Routes();
let model = new Model(app);

routes.publish(app,controller);

module.exports = app;
