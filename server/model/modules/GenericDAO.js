class GenericDAO{

    constructor(model){
        this._model = model;
    }

    findAll(cb){
        this._model.find({}).sort({
                name: 1
        }).exec(cb);
    }

    findById(id,cb){
        this._model.findOne({_id:id}).exec(cb);
    }

    save(object,cb){
        object = new this._model(object);
        object.save();
        return object;
    }

    get model(){
        return this._model;
    }
}

export default GenericDAO;