import GenericDAO from "../GenericDAO"

class ContactDAO extends GenericDAO{

    constructor(contact){
        super(contact);
    }

    findByName(name,cb){
        let pattern = new RegExp(".*"+name+".*","i");
        this.model.find({"name": pattern}, cb);
    }
}

export default ContactDAO;