import mongoose from "Mongoose"
import assert from "assert"

let Schema = mongoose.Schema;

let contactSchema = new Schema({
    name: String,
    email: String,
    phone: String,
    addresses: [{}],
    tags: {
        type: [String],
        index:true,
        lowercase:true
    }
});

contactSchema.pre('save', function(next) {
  this.tags = this.name.split(" ");
  
  next();
});

let Contact = mongoose.model('Contact', contactSchema);

export default Contact