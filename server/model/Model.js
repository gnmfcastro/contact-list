import mongoose from "Mongoose"
import bluebird from "bluebird"
import Contact  from "./modules/Contact/Contact"

class Model{

    constructor(app){

        this._URI = app.config.DBHost;
        this._OPTIONS = {
           useMongoClient: true,
           promiseLibrary: bluebird
        }

        this.configure();
    }

    configure(){
       mongoose.set('debug', true);
       mongoose.connect(this.URI, this.OPTIONS)
       .then(db => this.onOpen(db))
       .catch(err => this.onError(err));
    }

    onOpen(db){
        Contact.find({}, function(err, users){
            if(users.length <= 0){
                let startJson = require('./startData.json');
                
                Contact.collection.insertMany(startJson, function(err, r){
                    console.log('START DATA INSERTED');
                });
            }
        });
    }

    onError(err){
        console.error(err);
    }

    get URI(){
        return this._URI;
    }

    get OPTIONS(){
        return this._OPTIONS;
    }

}

export default Model;