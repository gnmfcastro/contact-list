# Contact-list


## Configuration Setup

1. Be sure to have installed (Node, Mongodb and NPM)
2. Enter in the folder client and execute ```npm install```
3. Enter in the folder server and execute ```npm install```
3. In the server folder be sure the db folder was created if not create it

## Running Server Application
1. Enter in server folder and execute ```npm run db``` for MacOSX/ Linux or execute ```mongod --dbpath=db``` for windows
2. Enter in server folder and execute ```npm start```

* If you're in MacOS/ Linux or Unix based System you can stop mongo ```npm run killdb```

## Running FrontEnd Application

1. Enter in client folder and execute ```npm start```
 
## Runtest FrontEnd Application

1. Enter in client folder and execute ```npm test```

## Runtest FrontEnd Application

1. Enter in server folder and execute ```npm test```