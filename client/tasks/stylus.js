var gulp        = require('gulp');
var stylus      = require("gulp-stylus");

gulp.task('stylus', function() {
    return gulp.src("static/assets/css/main.styl")
        .pipe(stylus())
        .pipe(gulp.dest("static/assets/css"));
});