var gulp        = require("gulp");
var browserSync = require('browser-sync').create();

gulp.task('serve', ['stylus'], function(){

    browserSync.init({
        server: "."
    });

    gulp.watch("static/assets/css/**/*.styl", ["stylus"]);
    gulp.watch("static/assets/css/main.css").on("change", browserSync.reload);
    gulp.watch("static/app/**/*.html").on("change", browserSync.reload);

});