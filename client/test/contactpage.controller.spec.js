describe("ContactpageController", function(){    

    beforeEach(
        module("app")
    );

    var $controller, $stateParams, $state, ContactService, $httpBackend, CONFIG, createController;

    beforeEach(inject(function(_$controller_, _$stateParams_ , _$httpBackend_ ,_$state_, _ContactService_, _CONFIG_){
        $controller = _$controller_;
        $state = _$state_;
        $stateParams = _$stateParams_;
        $httpBackend = _$httpBackend_;
        ContactService = _ContactService_;
        CONFIG = _CONFIG_;
        createController =  function() {
            return $controller('ContactpageController', {
                '$state': $state,
                '$stateParams': $stateParams, 
                'ContactService': ContactService
            });
        };
    }));

    afterEach(function(){
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    })

    describe('#onInit', function () {
        it('should id from state', function () {
            $stateParams.contactId = 123;

            vm = createController();
            vm.$onInit();

            expect(vm.contactId).toEqual(123);
        });

        it('should load contact by id', function () {
            $stateParams.contactId = 123;

            $httpBackend.whenRoute('GET', CONFIG.apiPath + '/contact/id/123').respond(function(){
                return [200, {name: 'Guilherme'}];
            });

            vm = createController();
            vm.$onInit();

            $httpBackend.flush();
         
            expect(vm.contact.name).toEqual("Guilherme");
        });
    });

});