describe("SearchbarController", function(){    

    beforeEach(
        module("app")
    );

    var $controller, ContactService, $httpBackend, CONFIG, createController;

    beforeEach(inject(function(_$controller_, _$httpBackend_ , _ContactService_, _CONFIG_){
        $controller = _$controller_;
        $httpBackend = _$httpBackend_;
        ContactService = _ContactService_;
        CONFIG = _CONFIG_;
        createController =  function() {
            return $controller('SearchbarController', {
                'ContactService': ContactService
            });
        };
        createParentController =  function() {
            return $controller('ContactListController', {
            });
        };
    }));

    afterEach(function(){
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    })

    describe('#onChangeName', function () {
        it('should call getall() service if vm.name length equals 0', function () {

            spyOn(ContactService, 'getAll').and.callThrough();

            vm = createController();
            vm.$onInit();

            vm.contactListCtrl = createParentController();
            vm.contactListCtrl.$onInit();
            
            vm.name = "";
            vm.onChangeName();

            expect(ContactService.getAll).toHaveBeenCalled();
        });

        it('should call getByName() service if vm.name length > 0', function () {

            spyOn(ContactService, 'getByName').and.callThrough();

            vm = createController();
            vm.$onInit();

            vm.contactListCtrl = createParentController();
            vm.contactListCtrl.$onInit();
            
            vm.name = "G";
            vm.onChangeName();

            expect(ContactService.getByName).toHaveBeenCalled();
        });

        it('should call setResultList with empty arr if result of service get and status different of 2XX', function () {

            $httpBackend.whenRoute('GET', CONFIG.apiPath + '/contacts').respond(function(){
                return [404];
            });

            $httpBackend.whenRoute('GET', CONFIG.apiPath + '/contact/G').respond(function(){
                return [404];
            });

            vm = createController();
            vm.$onInit();

            vm.contactListCtrl = createParentController();
            vm.contactListCtrl.$onInit();
            
            vm.name = "G";
            vm.onChangeName();

            expect(vm.contactListCtrl.list).toEqual([]);

            vm.name = "";
            vm.onChangeName();

            expect(vm.contactListCtrl.list).toEqual([]);

            $httpBackend.flush();
        });

        it('should call setResultList with response arr if result of service get and status equals 200', function () {

            $httpBackend.whenRoute('GET', CONFIG.apiPath + '/contacts').respond(function(){
                return [200, [{name: "Guilherme"}, {name: "Gui"}, {name: "Gustavo"}]];
            });

            $httpBackend.whenRoute('GET', CONFIG.apiPath + '/contact/G').respond(function(){
                return [200, [{name: "Guilherme"}, {name: "Gui"}]];
            });

            vm = createController();
            vm.$onInit();

            vm.contactListCtrl = createParentController();
            vm.contactListCtrl.$onInit();
            
            vm.name = "G";
            vm.onChangeName();

            $httpBackend.flush();

            expect(vm.contactListCtrl.list).toEqual([{name: "Guilherme"}, {name: "Gui"}]);

            vm.name = "";
            vm.onChangeName();

            $httpBackend.flush();

            expect(vm.contactListCtrl.list).toEqual([{name: "Guilherme"}, {name: "Gui"}, {name: "Gustavo"}]);
        });
    });

});