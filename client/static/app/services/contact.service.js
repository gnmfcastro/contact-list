(function(){
    "use strict'"

    angular.module("app").service("ContactService", ContactService);

    ContactService.$inject = ['$http', 'CONFIG'];

    function ContactService($http, CONFIG){
        this.getAll = function(){
            return $http({
                url: CONFIG.apiPath + '/contacts',
				method: 'GET'
            });
        },

        this.getByName = function(name){
            return $http({
                url: CONFIG.apiPath + '/contact/' + name ,
				method: 'GET'
            });
        }

        this.getById = function(id){
            return $http({
                url: CONFIG.apiPath + '/contact/id/' + id ,
				method: 'GET'
            });
        }

        this.newContact = function(contact){
            contact.addresses = [contact.address];

            var data = JSON.stringify(contact);

            return $http({
                url: CONFIG.apiPath + '/contact',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: data
            });
        }
    }

})();