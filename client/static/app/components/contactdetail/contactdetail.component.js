(function(){
    "use strict";

    angular.module("app").component('contactDetail',{
        trasnclude: true,
        bindings:{
            contact: "<"
        },
        controller: "ContactDetailController",
        controllerAs: "vm",
        templateUrl: "static/app/components/contactdetail/contactdetail.html"
    });

})();