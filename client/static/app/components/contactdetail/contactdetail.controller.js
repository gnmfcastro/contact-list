(function(){
    "use strict";

    angular.module("app").controller("ContactDetailController", ContactDetailController);

    ContactDetailController.inject = ["$state"];

    function ContactDetailController($state){
        var vm = this;
        vm.seeMore = seeMore;

        vm.$onInit = function(){
        }

        function seeMore(){
            $state.go("contactpage", {contactId: vm.contact._id});
        }

    }

})();