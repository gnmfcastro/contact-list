(function(){
    "use strict";

    angular.module("app").controller("ContactListController", ContactListController);

    ContactListController.inject = [];

    function ContactListController(){
        var vm = this;

        vm.$onInit = function(){
            vm.list = [];
            vm.setResultList=setResultList;
            vm.getResultList=getResultList;
        }

        function setResultList(list){
            vm.list = list;
        }

        function getResultList(){
            return vm.list;
        }

    }

})();