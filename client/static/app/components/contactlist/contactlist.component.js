(function(){
    "use strict";

    angular.module("app").component('contactlist',{
        controller: "ContactListController",
        controllerAs: "vm",
        templateUrl: "static/app/components/contactlist/contactlist.html"
    });


})();