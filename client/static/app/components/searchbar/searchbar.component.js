(function(){
    "use strict";

    angular.module("app").component('searchBar',{
        transclude: true,
        require:{
            contactListCtrl: "^contactlist"
        },
        controller: "SearchbarController",
        controllerAs: "vm",
        templateUrl: "static/app/components/searchbar/searchbar.html"
    });


})();