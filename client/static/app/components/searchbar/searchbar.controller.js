(function(){
    "use strict";

    angular.module("app").controller("SearchbarController", SearchbarController);

    SearchbarController.inject = ["ContactService"];

    function SearchbarController(ContactService){
        var vm = this;

        vm.$onInit = function(){
            vm.name = "";
            vm.isAdding = false;
            vm.newcontact = {
                name: "",
                phone: "",
                email: "",
                address: ""
            };

            vm.getName = getName;
            vm.onChangeName = onChangeName;
            vm.addContact = addContact;
            vm.toggleAddContact = toggleAddContact;

            onChangeName();
        }
       
        function onChangeName(){
            if(vm.name.length != 0){
                ContactService.getByName(vm.name).then(function(res){
                    vm.contactListCtrl.setResultList(res.data);
                })
                .catch(function(err){
                    vm.contactListCtrl.setResultList([]);
                });
            }else{
                ContactService.getAll().then(function(res){
                    vm.contactListCtrl.setResultList(res.data);
                })
                .catch(function(err){
                    vm.contactListCtrl.setResultList([]);
                });
            }
        }

        function getName(){
            return vm.name;
        }

        function addContact(){
            ContactService.newContact(vm.newcontact).then(function(res){
                alert("Registred !");
                toggleAddContact();
                onChangeName();
            })
            .catch(function(err){ 
                alert("Error !");
            });

        }

        function toggleAddContact(){
            resetContact();
            vm.isAdding = !vm.isAdding;
        }

        function resetContact(){
            vm.newcontact = {
                name: "",
                phone: "",
                email: "",
                address: ""
            };
        }
        
    }

})();