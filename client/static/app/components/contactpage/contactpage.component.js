(function(){
    "use strict";

    angular.module("app").component('contactpage',{
        transclude: true,
        controller: "ContactpageController",
        controllerAs: "vm",
        templateUrl: "static/app/components/contactpage/contactpage.html"
    });


})();