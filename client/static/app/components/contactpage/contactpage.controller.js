(function(){
    "use strict";

    angular.module("app").controller("ContactpageController", ContactpageController);

    ContactpageController.inject = ["ContactService", "$state","$stateParams"];

    function ContactpageController(ContactService,$state,$stateParams){
        var vm = this;

        vm.$onInit = function(){

            vm.contactId = $stateParams.contactId;
            vm.contact = {};

            ContactService.getById(vm.contactId).then(function(res){
                vm.contact = res.data; 
            })
            .catch(function(err){
                $state.go("contactlist");
            })
        }

    }

})();