"use strict";

angular
    .module("app")
    .config(Config);

Config.$inject = ['$stateProvider', '$urlRouterProvider'];

function Config($stateProvider, $urlRouterProvider){

    $urlRouterProvider.otherwise('/');

    $stateProvider.state('contactlist',{
        url:'/',
        component:'contactlist'
    })
    .state('contactpage',{
        url:'/contact/:contactId',
        component:'contactpage'
    });

}